defmodule Advent05 do
  @moduledoc """
  Documentation for `Advent05`.
  """

  @doc """
  Checks a value against a given spec.

  ## Examples

      iex> Advent05.value_to_spec(79, "50 98 2
      ...>                             52 50 48")
      81

      iex> Advent05.value_to_spec(14, "50 98 2
      ...>                             52 50 48")
      14

      iex> Advent05.value_to_spec(55, "50 98 2
      ...>                             52 50 48")
      57

      iex> Advent05.value_to_spec(13, "50 98 2
      ...>                             52 50 48")
      13

      iex> Advent05.value_to_spec(79, "50 98 2
      ...>                             52 50 48") |> Advent05.value_to_spec("0 15 37
      ...>                                                                  37 52 2
      ...>                                                                  39 0 15")
      81

      iex> Advent05.value_to_spec(79, "50 98 2
      ...>                             52 50 48") |> Advent05.value_to_spec("0 15 37
      ...>                                                                  37 52 2
      ...>                                                                  39 0 15") |> Advent05.value_to_spec("49 53 8
      ...>                                                                                                       0 11 42
      ...>                                                                                                       42 0 7
      ...>                                                                                                       57 7 4")
      81

      iex> Advent05.value_to_spec(79, "50 98 2
      ...>                             52 50 48") |> Advent05.value_to_spec("0 15 37
      ...>                                                                  37 52 2
      ...>                                                                  39 0 15") |> Advent05.value_to_spec("49 53 8
      ...>                                                                                                       0 11 42
      ...>                                                                                                       42 0 7
      ...>                                                                                                       57 7 4") |> Advent05.value_to_spec("88 18 7
      ...>                                                                                                                                           18 25 70")
      74

      iex> Advent05.value_to_spec(79, "50 98 2
      ...>                             52 50 48") |> Advent05.value_to_spec("0 15 37
      ...>                                                                  37 52 2
      ...>                                                                  39 0 15") |> Advent05.value_to_spec("49 53 8
      ...>                                                                                                       0 11 42
      ...>                                                                                                       42 0 7
      ...>                                                                                                       57 7 4") |> Advent05.value_to_spec("88 18 7
      ...>                                                                                                                                           18 25 70") |> Advent05.value_to_spec("45 77 23
      ...>                                                                                                                                                                                 81 45 19
      ...>                                                                                                                                                                                 68 64 13")
      78

      iex> Advent05.value_to_spec(79, "50 98 2
      ...>                             52 50 48") |> Advent05.value_to_spec("0 15 37
      ...>                                                                  37 52 2
      ...>                                                                  39 0 15") |> Advent05.value_to_spec("49 53 8
      ...>                                                                                                       0 11 42
      ...>                                                                                                       42 0 7
      ...>                                                                                                       57 7 4") |> Advent05.value_to_spec("88 18 7
      ...>                                                                                                                                           18 25 70") |> Advent05.value_to_spec("45 77 23
      ...>                                                                                                                                                                                 81 45 19
      ...>                                                                                                                                                                                 68 64 13") |> Advent05.value_to_spec("0 69 1
      ...>                                                                                                                                                                                                                       1 0 69")
      78

      iex> Advent05.value_to_spec(79, "50 98 2
      ...>                             52 50 48") |> Advent05.value_to_spec("0 15 37
      ...>                                                                  37 52 2
      ...>                                                                  39 0 15") |> Advent05.value_to_spec("49 53 8
      ...>                                                                                                       0 11 42
      ...>                                                                                                       42 0 7
      ...>                                                                                                       57 7 4") |> Advent05.value_to_spec("88 18 7
      ...>                                                                                                                                           18 25 70") |> Advent05.value_to_spec("45 77 23
      ...>                                                                                                                                                                                 81 45 19
      ...>                                                                                                                                                                                 68 64 13") |> Advent05.value_to_spec("0 69 1
      ...>                                                                                                                                                                                                                       1 0 69") |> Advent05.value_to_spec("60 56 37
      ...>                                                                                                                                                                                                                                                           56 93 4")
      82

      iex> Advent05.value_to_spec(14, "50 98 2
      ...>                             52 50 48") |>
      ...>     Advent05.value_to_spec("0 15 37
      ...>                             37 52 2
      ...>                             39 0 15") |>
      ...>     Advent05.value_to_spec("49 53 8
      ...>                             0 11 42
      ...>                             42 0 7
      ...>                             57 7 4") |>
      ...>     Advent05.value_to_spec("88 18 7
      ...>                             18 25 70") |>
      ...>     Advent05.value_to_spec("45 77 23
      ...>                             81 45 19
      ...>                             68 64 13") |>
      ...>     Advent05.value_to_spec("0 69 1
      ...>                             1 0 69") |>
      ...>     Advent05.value_to_spec("60 56 37
      ...>                             56 93 4")
      43

      iex> Advent05.value_to_spec(55, "50 98 2
      ...>                             52 50 48") |>
      ...>     Advent05.value_to_spec("0 15 37
      ...>                             37 52 2
      ...>                             39 0 15") |>
      ...>     Advent05.value_to_spec("49 53 8
      ...>                             0 11 42
      ...>                             42 0 7
      ...>                             57 7 4") |>
      ...>     Advent05.value_to_spec("88 18 7
      ...>                             18 25 70") |>
      ...>     Advent05.value_to_spec("45 77 23
      ...>                             81 45 19
      ...>                             68 64 13") |>
      ...>     Advent05.value_to_spec("0 69 1
      ...>                             1 0 69") |>
      ...>     Advent05.value_to_spec("60 56 37
      ...>                             56 93 4")
      86

      iex> Advent05.value_to_spec(13, "50 98 2
      ...>                             52 50 48") |>
      ...>     Advent05.value_to_spec("0 15 37
      ...>                             37 52 2
      ...>                             39 0 15") |>
      ...>     Advent05.value_to_spec("49 53 8
      ...>                             0 11 42
      ...>                             42 0 7
      ...>                             57 7 4") |>
      ...>     Advent05.value_to_spec("88 18 7
      ...>                             18 25 70") |>
      ...>     Advent05.value_to_spec("45 77 23
      ...>                             81 45 19
      ...>                             68 64 13") |>
      ...>     Advent05.value_to_spec("0 69 1
      ...>                             1 0 69") |>
      ...>     Advent05.value_to_spec("60 56 37
      ...>                             56 93 4")
      35

  """
  @spec value_to_spec(integer(), String.t() | list(tuple())) :: integer()
  def value_to_spec(seed, spec) when is_list(spec) do
    check(seed, spec)
  end

  def value_to_spec(seed, spec_str) do
    value_to_spec(seed, str_to_spec(spec_str))
  end

  defp str_to_spec(spec) do
    String.split(spec, "\n", trim: true) |> do_str_to_spec([])
  end

  defp do_str_to_spec([], spec), do: spec |> List.keysort(1)

  defp do_str_to_spec([s | more_s], spec) do
    [destination, source, length] = String.split(s) |> Enum.map(&String.to_integer(&1))
    do_str_to_spec(more_s, [{source, source + length - 1, destination - source} | spec])
  end

  defp check(seed, []), do: seed

  defp check(seed, [{start, finish, offset} | _more_spec]) when seed >= start and seed <= finish,
    do: seed + offset

  defp check(seed, [_ | more_spec]), do: check(seed, more_spec)

  @doc """
  Checks a set of values against a full spec.

  """
  @spec lowest_location(:units | :ranges) :: integer()
  def lowest_location(:units) do
    Utils.get_input("priv/seeds", &String.split(&1))
    |> hd()
    |> Enum.map(&String.to_integer(&1))
    |> do_lowest_location()
  end

  def lowest_location(:ranges) do
    do_lowest_location([485_317_202, 73_175_807])
  end

  defp do_lowest_location([start, length]) do
    Enum.to_list(start..(start + length - 1)) |> do_lowest_location()
  end

  defp do_lowest_location(seeds) do
    seeds_to_soil = Utils.get_input("priv/seed-to-soil") |> Enum.join("\n") |> str_to_spec()

    soil_to_fertilizer =
      Utils.get_input("priv/soil-to-fertilizer") |> Enum.join("\n") |> str_to_spec()

    fertilizer_to_water =
      Utils.get_input("priv/fertilizer-to-water") |> Enum.join("\n") |> str_to_spec()

    water_to_light = Utils.get_input("priv/water-to-light") |> Enum.join("\n") |> str_to_spec()

    light_to_temperature =
      Utils.get_input("priv/light-to-temperature") |> Enum.join("\n") |> str_to_spec()

    temperature_to_humidity =
      Utils.get_input("priv/temperature-to-humidity") |> Enum.join("\n") |> str_to_spec()

    humidity_to_location =
      Utils.get_input("priv/humidity-to-location") |> Enum.join("\n") |> str_to_spec()

    Enum.map(seeds, fn s ->
      value_to_spec(s, seeds_to_soil)
      |> value_to_spec(soil_to_fertilizer)
      |> value_to_spec(fertilizer_to_water)
      |> value_to_spec(water_to_light)
      |> value_to_spec(light_to_temperature)
      |> value_to_spec(temperature_to_humidity)
      |> value_to_spec(humidity_to_location)
    end)
    |> Enum.sort()
    |> hd()
  end
end
