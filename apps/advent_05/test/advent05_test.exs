defmodule Advent05Test do
  use ExUnit.Case
  doctest Advent05

  test "answer to first challenge" do
    answer = Advent05.lowest_location(:units)

    assert answer == 3_374_647
  end

  @tag :skip
  test "answer to second challenge" do
    answer = Advent05.lowest_location(:ranges)

    assert answer == 6_082_852
  end
end
