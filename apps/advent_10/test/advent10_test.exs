defmodule Advent10Test do
  use ExUnit.Case
  doctest Advent10

  test "answer to first challenge" do
    map_str = Utils.get_input("priv/input", fn s -> String.codepoints(s) end)
    answer = Advent10.length(128, 36, map_str)

    assert answer == 13_712
  end

  @tag :skip
  test "answer to second challenge" do
    answer = Utils.get_input("priv/input")

    assert answer == ["TBD"]
  end
end
