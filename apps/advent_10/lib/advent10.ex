defmodule Advent10 do
  @moduledoc """
  Documentation for `Advent10`.
  """

  @doc """
  Calculates loop length, given a pipeline map.

  ## Examples

      iex> Advent10.length(1, 1, [[".", ".", ".", ".", "."],
      ...>                        [".", "S", "-", "7", "."],
      ...>                        [".", "|", ".", "|", "."],
      ...>                        [".", "L", "-", "J", "."],
      ...>                        [".", ".", ".", ".", "."]])
      8

      iex> Advent10.length(2, 0, [[".", ".", "F", "7", "."],
      ...>                        [".", "F", "J", "|", "."],
      ...>                        ["S", "J", ".", "L", "7"],
      ...>                        ["|", "F", "-", "-", "J"],
      ...>                        ["L", "J", ".", ".", "."]])
      16

  """
  @spec length(non_neg_integer(), non_neg_integer(), list(String.t())) :: integer()
  def length(x, y, map_str) do
    process_map_spec(0, 0, map_str, %{})
    |> transverse_map(x, y)
  end

  defp process_map_spec(_, _, [], map), do: map

  defp process_map_spec(x, y = 0, [row | more_rows], map) do
    rmap = process_row(x, y, row, %{})
    cmap = Map.put_new(map, x, rmap)
    process_map_spec(x + 1, 0, more_rows, cmap)
  end

  defp process_row(_, _, [], map), do: map

  defp process_row(x, y, [v | more_v], map) do
    vmap = Map.put_new(map, y, v)
    process_row(x, y + 1, more_v, vmap)
  end

  defp transverse_map(map, x, y), do: do_transverse_map(map, :south, x + 1, y, map[x + 1][y], 1)

  defp do_transverse_map(_, _, _, _, "S", steps), do: steps

  defp do_transverse_map(map, :south, x, y, "|", steps),
    do: do_transverse_map(map, :south, x + 1, y, map[x + 1][y], steps + 1)

  defp do_transverse_map(map, :north, x, y, "|", steps),
    do: do_transverse_map(map, :north, x - 1, y, map[x - 1][y], steps + 1)

  defp do_transverse_map(map, :south, x, y, "L", steps),
    do: do_transverse_map(map, :east, x, y + 1, map[x][y + 1], steps + 1)

  defp do_transverse_map(map, :west, x, y, "L", steps),
    do: do_transverse_map(map, :north, x - 1, y, map[x - 1][y], steps + 1)

  defp do_transverse_map(map, :east, x, y, "-", steps),
    do: do_transverse_map(map, :east, x, y + 1, map[x][y + 1], steps + 1)

  defp do_transverse_map(map, :west, x, y, "-", steps),
    do: do_transverse_map(map, :west, x, y - 1, map[x][y - 1], steps + 1)

  defp do_transverse_map(map, :east, x, y, "J", steps),
    do: do_transverse_map(map, :north, x - 1, y, map[x - 1][y], steps + 1)

  defp do_transverse_map(map, :south, x, y, "J", steps),
    do: do_transverse_map(map, :west, x, y - 1, map[x][y - 1], steps + 1)

  defp do_transverse_map(map, :north, x, y, "7", steps),
    do: do_transverse_map(map, :west, x, y - 1, map[x][y - 1], steps + 1)

  defp do_transverse_map(map, :east, x, y, "7", steps),
    do: do_transverse_map(map, :south, x + 1, y, map[x + 1][y], steps + 1)

  defp do_transverse_map(map, :west, x, y, "F", steps),
    do: do_transverse_map(map, :south, x + 1, y, map[x + 1][y], steps + 1)

  defp do_transverse_map(map, :north, x, y, "F", steps),
    do: do_transverse_map(map, :east, x, y + 1, map[x][y + 1], steps + 1)
end
