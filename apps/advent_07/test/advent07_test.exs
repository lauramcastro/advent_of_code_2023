defmodule Advent07Test do
  use ExUnit.Case
  doctest Advent07

  test "answer to first challenge" do
    answer =
      Utils.get_input(
        "priv/input",
        fn s ->
          [cards, bid] = String.split(s)
          {cards, String.to_integer(bid)}
        end
      )
      |> Advent07.camel_poker_winnings()

    assert answer == 254_024_898
  end

  test "answer to second challenge" do
    jokers = true

    answer =
      Utils.get_input(
        "priv/input",
        fn s ->
          [cards, bid] = String.split(s)
          {cards, String.to_integer(bid)}
        end
      )
      |> Advent07.camel_poker_winnings(jokers)

    assert answer == 254_115_617
  end
end
