defmodule Advent07 do
  @moduledoc """
  Documentation for `Advent07`.
  """

  @doc """
  Orders a list of hands of "camel poker".

  ## Examples

      iex> Advent07.camel_poker_order([{"32T3K", 765}, {"T55J5", 684}, {"KK677", 28}, {"KTJJT", 220}, {"QQQJA", 483}])
      [{"32T3K", 765}, {"KTJJT", 220}, {"KK677", 28}, {"T55J5", 684}, {"QQQJA", 483}]

      iex> Advent07.camel_poker_order([{"32T3K", 765}, {"T55J5", 684}, {"KK677", 28}, {"KTJJT", 220}, {"QQQJA", 483}], true)
      [{"32T3K", 765}, {"KK677", 28}, {"T55J5", 684}, {"QQQJA", 483}, {"KTJJT", 220}]

  """
  @spec camel_poker_order(list({String.t(), integer()}), boolean()) ::
          list({String.t(), integer()})
  def camel_poker_order(hands, jokers \\ false)

  def camel_poker_order(hands, jokers) do
    hands
    |> Enum.map(fn {hand, bid} ->
      cards = String.codepoints(hand)

      hand_type =
        cards |> Enum.sort(&caml_poker_card_order(&1, &2, jokers)) |> caml_poker_hand_type(jokers)

      {cards, hand_type, bid}
    end)
    |> Enum.sort(&camel_poker_hand_order(&1, &2, jokers))
    |> Enum.reverse()
    |> Enum.map(fn {cards, _type, bid} -> {Enum.join(cards), bid} end)
  end

  defp caml_poker_card_order(c1, c2, jokers) do
    cc1 =
      try do
        String.to_integer(c1)
      rescue
        _ -> c1
      end

    cc2 =
      try do
        String.to_integer(c2)
      rescue
        _ -> c2
      end

    do_caml_poker_card_order(cc1, cc2, jokers)
  end

  # without jokers: A, K, Q, J, T, 9, 8, 7, 6, 5, 4, 3, 2
  # with jokers:    A, K, Q, T, 9, 8, 7, 6, 5, 4, 3, 2, J
  defp do_caml_poker_card_order(c1, c2, _) when is_integer(c1) and is_integer(c2), do: c1 >= c2
  defp do_caml_poker_card_order(c, "J", true) when is_integer(c), do: true
  defp do_caml_poker_card_order(c1, _c2, _) when is_integer(c1), do: false
  defp do_caml_poker_card_order("J", c2, true) when is_integer(c2), do: false
  defp do_caml_poker_card_order(_c1, c2, _) when is_integer(c2), do: true
  defp do_caml_poker_card_order("A", _, _), do: true
  defp do_caml_poker_card_order("K", c, _) when c == "A", do: false
  defp do_caml_poker_card_order("K", _, _), do: true
  defp do_caml_poker_card_order("Q", c, _) when c == "A" or c == "K", do: false
  defp do_caml_poker_card_order("Q", _, _), do: true
  defp do_caml_poker_card_order("J", c, false) when c == "A" or c == "K" or c == "Q", do: false
  defp do_caml_poker_card_order("J", _, false), do: true
  defp do_caml_poker_card_order("J", _, true), do: false

  defp do_caml_poker_card_order("T", c, false) when c == "A" or c == "K" or c == "Q" or c == "J",
    do: false

  defp do_caml_poker_card_order("T", c, true) when c == "A" or c == "K" or c == "Q", do: false
  defp do_caml_poker_card_order("T", _, _), do: true

  defp caml_poker_hand_type(cards, true) do
    hand_type = caml_poker_hand_type(cards, false)
    improve(hand_type, Enum.count(cards, fn c -> c == "J" end))
  end

  defp caml_poker_hand_type([c, c, c, c, c], false), do: :five_of_a_kind
  defp caml_poker_hand_type([c, c, c, c, _], false), do: :four_of_a_kind
  defp caml_poker_hand_type([_, c, c, c, c], false), do: :four_of_a_kind
  defp caml_poker_hand_type([c1, c1, c1, c2, c2], false), do: :full_house
  defp caml_poker_hand_type([c1, c1, c2, c2, c2], false), do: :full_house
  defp caml_poker_hand_type([c, c, c, _, _], false), do: :three_of_a_kind
  defp caml_poker_hand_type([_, c, c, c, _], false), do: :three_of_a_kind
  defp caml_poker_hand_type([_, _, c, c, c], false), do: :three_of_a_kind
  defp caml_poker_hand_type([c1, c1, c2, c2, _], false), do: :two_pair
  defp caml_poker_hand_type([c1, c1, _, c2, c2], false), do: :two_pair
  defp caml_poker_hand_type([_, c1, c1, c2, c2], false), do: :two_pair
  defp caml_poker_hand_type([c, c, _, _, _], false), do: :one_pair
  defp caml_poker_hand_type([_, c, c, _, _], false), do: :one_pair
  defp caml_poker_hand_type([_, _, c, c, _], false), do: :one_pair
  defp caml_poker_hand_type([_, _, _, c, c], false), do: :one_pair
  defp caml_poker_hand_type(_, false), do: :high_card

  defp improve(whatever, 0), do: whatever
  defp improve(:five_of_a_kind, 5), do: :five_of_a_kind
  defp improve(:four_of_a_kind, 1), do: :five_of_a_kind
  defp improve(:four_of_a_kind, 4), do: :five_of_a_kind
  defp improve(:full_house, 2), do: :five_of_a_kind
  defp improve(:full_house, 3), do: :five_of_a_kind
  defp improve(:three_of_a_kind, 1), do: :four_of_a_kind
  defp improve(:three_of_a_kind, 3), do: :four_of_a_kind
  defp improve(:two_pair, 1), do: :full_house
  defp improve(:two_pair, 2), do: :four_of_a_kind
  defp improve(:one_pair, 1), do: :three_of_a_kind
  defp improve(:one_pair, 2), do: :three_of_a_kind
  defp improve(:high_card, 1), do: :one_pair

  defp camel_poker_hand_order({cards1, type, _}, {cards2, type, _}, jokers),
    do: do_caml_poker_hand_order(cards1, cards2, jokers)

  defp camel_poker_hand_order({_, :five_of_a_kind, _}, _, _), do: true

  defp camel_poker_hand_order({_, :four_of_a_kind, _}, {_, type, _}, _)
       when type == :five_of_a_kind,
       do: false

  defp camel_poker_hand_order({_, :four_of_a_kind, _}, _, _), do: true

  defp camel_poker_hand_order({_, :full_house, _}, {_, type, _}, _)
       when type == :five_of_a_kind or type == :four_of_a_kind,
       do: false

  defp camel_poker_hand_order({_, :full_house, _}, _, _), do: true

  defp camel_poker_hand_order({_, :three_of_a_kind, _}, {_, type, _}, _)
       when type == :five_of_a_kind or type == :four_of_a_kind or type == :full_house,
       do: false

  defp camel_poker_hand_order({_, :three_of_a_kind, _}, _, _), do: true

  defp camel_poker_hand_order({_, :two_pair, _}, {_, type, _}, _)
       when type == :one_pair or type == :high_card,
       do: true

  defp camel_poker_hand_order({_, :two_pair, _}, _, _), do: false

  defp camel_poker_hand_order({_, :one_pair, _}, {_, type, _}, _) when type == :high_card,
    do: true

  defp camel_poker_hand_order({_, :one_pair, _}, _, _), do: false
  defp camel_poker_hand_order({_, :high_card, _}, _, _), do: false

  # defp do_caml_poker_hand_order([], []), do: false
  defp do_caml_poker_hand_order([c | rest1], [c | rest2], jokers),
    do: do_caml_poker_hand_order(rest1, rest2, jokers)

  defp do_caml_poker_hand_order([c1 | _], [c2 | _], jokers),
    do: caml_poker_card_order(c1, c2, jokers)

  @doc """
  Calculates the winnings of a list of camel poker hands.

  ## Examples

      iex> Advent07.camel_poker_winnings([{"32T3K", 765}, {"T55J5", 684}, {"KK677", 28}, {"KTJJT", 220}, {"QQQJA", 483}])
      6440

      iex> Advent07.camel_poker_winnings([{"32T3K", 765}, {"T55J5", 684}, {"KK677", 28}, {"KTJJT", 220}, {"QQQJA", 483}], true)
      5905

  """
  @spec camel_poker_winnings(list({String.t(), integer()}), boolean()) :: integer()
  def camel_poker_winnings(hands, jokers \\ false)

  def camel_poker_winnings(hands, jokers) do
    hands
    |> Enum.map(fn {hand, bid} ->
      cards = String.codepoints(hand)

      hand_type =
        cards |> Enum.sort(&caml_poker_card_order(&1, &2, jokers)) |> caml_poker_hand_type(jokers)

      {cards, hand_type, bid}
    end)
    |> Enum.sort(&camel_poker_hand_order(&1, &2, jokers))
    |> calculate_winnings(length(hands), 0)
  end

  defp calculate_winnings([{_cards, _type, bid}], 1, acc), do: acc + bid

  defp calculate_winnings([{_cards, _type, bid} | rest], n, acc),
    do: calculate_winnings(rest, n - 1, acc + bid * n)
end
