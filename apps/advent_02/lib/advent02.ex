defmodule Advent02 do
  @moduledoc """
  Documentation for `Advent02`.
  """

  @doc """
  Determines if a game is possible, given a set of ball constraints.

  ## Examples


      iex> Advent02.possible_game("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green", [red: 12, green: 13, blue: 14])
      true

      iex> Advent02.possible_game("Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue", [red: 12, green: 13, blue: 14])
      true

      iex> Advent02.possible_game("Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red", [red: 12, green: 13, blue: 14])
      false

      iex> Advent02.possible_game("Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red", [red: 12, green: 13, blue: 14])
      false

      iex> Advent02.possible_game("Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green", [red: 12, green: 13, blue: 14])
      true

  """
  @spec possible_game(String.t(), Keyword.t()) :: boolean()
  def possible_game(game_description, constraints) do
    Regex.split(~r/Game (\d)+: /, game_description, trim: true)
    |> hd()
    |> String.split("; ", trim: true)
    |> do_possible_game(constraints)
  end

  defp do_possible_game([], _), do: true

  defp do_possible_game([hint | more_hints], constraints) do
    handful_of_colours = String.split(hint, ", ") |> Enum.map(&String.split(&1, " "))

    if Enum.all?(handful_of_colours, &possible?(&1, constraints)),
      do: do_possible_game(more_hints, constraints),
      else: false
  end

  defp possible?([value, "red"], red: red_value, green: _green_value, blue: _blue_value),
    do: String.to_integer(value) <= red_value

  defp possible?([value, "green"], red: _red_value, green: green_value, blue: _blue_value),
    do: String.to_integer(value) <= green_value

  defp possible?([value, "blue"], red: _red_value, green: _green_value, blue: blue_value),
    do: String.to_integer(value) <= blue_value

  @doc """
  Calculates the sum of the IDs of possible games, given a set of ball constraints.

  ## Examples


      iex> Advent02.sum_possible_game_ids(["Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green",
      ...>                                 "Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue",
      ...>                                 "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red",
      ...>                                 "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red",
      ...>                                 "Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"],
      ...>                                [red: 12, green: 13, blue: 14])
      8

  """
  @spec sum_possible_game_ids(list(String.t()), Keyword.t()) :: integer()
  def sum_possible_game_ids(games, constraints) do
    games
    |> Enum.filter(&possible_game(&1, constraints))
    |> Enum.reduce(
      0,
      fn game, acc ->
        [_, value] = Regex.run(~r/Game (\d+)/, game)
        String.to_integer(value) + acc
      end
    )
  end

  @doc """
  Determines the minimum set of balls for a game.

  ## Examples


      iex> Advent02.minimum_set("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green")
      [red: 4, green: 2, blue: 6]

      iex> Advent02.minimum_set("Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue")
      [red: 1, green: 3, blue: 4]

      iex> Advent02.minimum_set("Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red")
      [red: 20, green: 13, blue: 6]

      iex> Advent02.minimum_set("Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red")
      [red: 14, green: 3, blue: 15]

      iex> Advent02.minimum_set("Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green")
      [red: 6, green: 3, blue: 2]

  """
  @spec minimum_set(String.t()) :: Keyword.t()
  def minimum_set(game_description) do
    Regex.split(~r/Game (\d)+: /, game_description, trim: true)
    |> hd()
    |> String.split("; ", trim: true)
    |> do_minimum_set(red: 0, green: 0, blue: 0)
  end

  defp do_minimum_set([], minimum_set), do: minimum_set

  defp do_minimum_set([hint | more_hints], minimum_set) do
    increased_m =
      String.split(hint, ", ")
      |> Enum.map(&String.split(&1, " "))
      |> increase_minimum(minimum_set)

    do_minimum_set(more_hints, increased_m)
  end

  defp increase_minimum([], m), do: m

  defp increase_minimum(
         [[value, "red"] | more_values],
         m = [red: red_value, green: green_value, blue: blue_value]
       ) do
    new_red = String.to_integer(value)

    new_m =
      if new_red > red_value, do: [red: new_red, green: green_value, blue: blue_value], else: m

    increase_minimum(more_values, new_m)
  end

  defp increase_minimum(
         [[value, "green"] | more_values],
         m = [red: red_value, green: green_value, blue: blue_value]
       ) do
    new_green = String.to_integer(value)

    new_m =
      if new_green > green_value,
        do: [red: red_value, green: new_green, blue: blue_value],
        else: m

    increase_minimum(more_values, new_m)
  end

  defp increase_minimum(
         [[value, "blue"] | more_values],
         m = [red: red_value, green: green_value, blue: blue_value]
       ) do
    new_blue = String.to_integer(value)

    new_m =
      if new_blue > blue_value, do: [red: red_value, green: green_value, blue: new_blue], else: m

    increase_minimum(more_values, new_m)
  end

  @doc """
  Calculates the sum of the powers of the minimum sets for given games.

  ## Examples


      iex> Advent02.sum_of_powers(["Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green",
      ...>                         "Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue",
      ...>                         "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red",
      ...>                         "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red",
      ...>                         "Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"])
      2286

  """
  @spec sum_of_powers(list(String.t())) :: integer()
  def sum_of_powers(games) do
    games
    |> Enum.map(&minimum_set(&1))
    |> Enum.reduce(0, fn [red: r, green: g, blue: b], acc -> r * g * b + acc end)
  end
end
