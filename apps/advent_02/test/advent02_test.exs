defmodule Advent02Test do
  use ExUnit.Case
  doctest Advent02

  test "answer to first challenge" do
    answer =
      Utils.get_input("priv/input")
      |> Advent02.sum_possible_game_ids(red: 12, green: 13, blue: 14)

    assert answer == 1_853
  end

  test "answer to second challenge" do
    answer = Utils.get_input("priv/input") |> Advent02.sum_of_powers()
    assert answer == 72_706
  end
end
