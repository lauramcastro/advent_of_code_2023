defmodule Advent08Test do
  use ExUnit.Case
  doctest Advent08

  test "answer to first challenge" do
    [directions] = Utils.get_input("priv/directions")
    map = Utils.get_input("priv/map")

    answer = Advent08.steps(directions, map)

    assert answer == 17_873
  end

  test "answer to second challenge" do
    [directions] = Utils.get_input("priv/directions")
    map = Utils.get_input("priv/map")

    answer = Advent08.ghost_steps(directions, map)

    assert answer == 15_746_133_679_061
  end
end
