defmodule Advent08 do
  @moduledoc """
  Documentation for `Advent08`.
  """

  @doc """
  Counts the steps to reach a "ZZZ" node from a "AAA" node in a given list of nodes,
  following a course of "L" and "R" directions.

  ## Examples

      iex> Advent08.steps("RL", ["AAA = (BBB, CCC)",
      ...>                       "BBB = (DDD, EEE)",
      ...>                       "CCC = (ZZZ, GGG)",
      ...>                       "DDD = (DDD, DDD)",
      ...>                       "EEE = (EEE, EEE)",
      ...>                       "GGG = (GGG, GGG)",
      ...>                       "ZZZ = (ZZZ, ZZZ)"])
      2


      iex> Advent08.steps("LLR", ["AAA = (BBB, BBB)",
      ...>                        "BBB = (AAA, ZZZ)",
      ...>                        "ZZZ = (ZZZ, ZZZ)"])
      6

  """
  @spec steps(String.t(), list(String.t())) :: integer()
  def steps(directions_str, map_str) do
    directions = String.codepoints(directions_str)
    map = parse_map(map_str, %{})
    follow_map("AAA", directions, directions, map, 0)
  end

  defp parse_map([], map), do: map

  defp parse_map([node | more_nodes], map) do
    [origin, destinations] = String.split(node, " = ")

    [left, right] =
      destinations |> String.trim_leading("(") |> String.trim_trailing(")") |> String.split(", ")

    parse_map(more_nodes, Map.put_new(map, origin, %{"L" => left, "R" => right}))
  end

  defp follow_map("ZZZ", [], _directions, _map, steps), do: steps

  defp follow_map(origin, [], directions, map, steps),
    do: follow_map(origin, directions, directions, map, steps)

  defp follow_map(origin, [direction | more_steps], directions, map, steps) do
    follow_map(map[origin][direction], more_steps, directions, map, steps + 1)
  end

  @doc """
  Counts the steps to reach a "__Z" node from all "__A" nodes in a given list of nodes at once,
  following a course of "L" and "R" directions.

  ## Examples

      iex> Advent08.ghost_steps("LR", ["11A = (11B, XXX)",
      ...>                             "11B = (XXX, 11Z)",
      ...>                             "11Z = (11B, XXX)",
      ...>                             "22A = (22B, XXX)",
      ...>                             "22B = (22C, 22C)",
      ...>                             "22C = (22Z, 22Z)",
      ...>                             "22Z = (22B, 22B)",
      ...>                             "XXX = (XXX, XXX)"])
      6

  """
  @spec ghost_steps(String.t(), list(String.t())) :: integer()
  def ghost_steps(directions_str, map_str) do
    directions = String.codepoints(directions_str)
    map = parse_map(map_str, %{})

    starting_points =
      Map.keys(map) |> Enum.filter(fn s -> String.codepoints(s) |> List.last() == "A" end)

    starting_points
    |> Enum.map(fn sp -> follow_ghost_map(sp, directions, directions, map, 0) end)
    |> Enum.reduce(1, fn st, acc -> Math.lcm(st, acc) end)
  end

  defp follow_ghost_map(origin, [], directions, map, steps),
    do: follow_ghost_map(origin, directions, directions, map, steps)

  defp follow_ghost_map(origin, [direction | more_steps], directions, map, steps),
    do:
      if(String.codepoints(origin) |> List.last() == "Z",
        do: steps,
        else: follow_ghost_map(map[origin][direction], more_steps, directions, map, steps + 1)
      )
end
