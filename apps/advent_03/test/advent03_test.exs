defmodule Advent03Test do
  use ExUnit.Case
  doctest Advent03

  test "answer to first challenge" do
    answer =
      Utils.get_input("priv/input", fn x ->
        String.codepoints(x)
        |> Enum.map(fn x ->
          try do
            String.to_integer(x)
          rescue
            _ -> x
          end
        end)
      end)
      |> Advent03.part_numbers()
      |> Enum.sum()

    assert answer == 531_561
  end

  test "answer to second challenge" do
    answer =
      Utils.get_input("priv/input", fn x ->
        String.codepoints(x)
        |> Enum.map(fn x ->
          try do
            String.to_integer(x)
          rescue
            _ -> x
          end
        end)
      end)
      |> Advent03.gear_ratio()

    assert answer == 83_279_367
  end
end
