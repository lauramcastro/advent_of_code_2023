defmodule Advent03 do
  @moduledoc """
  Documentation for `Advent03`.
  """

  @doc """
  Finds part numbers, given a schematic.

  ## Examples

      iex> Advent03.part_numbers([[ 4 , 6 , 7 ,".",".", 1 , 1 , 4 ,".","."],
      ...>                        [".",".",".","*",".",".",".",".",".","."],
      ...>                        [".",".", 3 , 5 ,".",".", 6 , 3 , 3 ,"."],
      ...>                        [".",".",".",".",".",".","#",".",".","."],
      ...>                        [ 6 , 1 , 7 ,"*",".",".",".",".",".","."],
      ...>                        [".",".",".",".",".","+",".", 5 , 8 ,"."],
      ...>                        [".",".", 5 , 9 , 2 ,".",".",".",".","."],
      ...>                        [".",".",".",".",".",".", 7 , 5 , 5 ,"."],
      ...>                        [".",".",".","$",".","*",".",".",".","."],
      ...>                        [".", 6 , 6 , 4 ,".", 5 , 9 , 8 ,".","."]])
      [35, 467, 592, 598, 617, 633, 664, 755]

      iex> Advent03.part_numbers([[ 4 , 6 , 7 ,".",".", 1 , 1 , 4 ,".","."],
      ...>                        [".",".",".","*",".",".",".",".",".","."],
      ...>                        [".",".", 3 , 5 ,".",".", 6 , 3 , 3 ,"."],
      ...>                        [".",".",".",".",".",".","#",".",".","."],
      ...>                        [ 6 , 1 , 7 ,"*",".",".",".",".",".","."],
      ...>                        [".",".",".",".",".","+",".", 5 , 8 ,"."],
      ...>                        [".",".", 5 , 9 , 2 ,".",".",".",".","."],
      ...>                        [".",".",".",".",".",".", 7 , 5 , 5 ,"."],
      ...>                        [".",".",".","$",".","*",".",".",".","."],
      ...>                        [".", 6 , 6 , 4 ,".", 5 , 9 , 8 ,".","."]]) |> Enum.sum()
      4361

  """
  @spec part_numbers(list(list(String.t()))) :: list(integer())
  def part_numbers(schematic) do
    {numbers, symbols} = locate_elements(schematic, 0, 0, [], [])

    filter_adjacent(numbers, symbols, [])
    |> Enum.sort()
    |> Enum.dedup()
    |> Enum.map(fn {n, _x, _y} -> n end)
  end

  defp locate_elements([], _x, _y, acc_numbers, acc_symbols), do: {acc_numbers, acc_symbols}

  defp locate_elements([[] | more_rows], x, _y, acc_numbers, acc_symbols),
    do: locate_elements(more_rows, x + 1, 0, acc_numbers, acc_symbols)

  defp locate_elements([[a, b, c | more_columns] | more_rows], x, y, acc_numbers, acc_symbols)
       when is_integer(a) and is_integer(b) and is_integer(c) do
    new_number = String.to_integer("#{a}#{b}#{c}")

    locate_elements(
      [more_columns | more_rows],
      x,
      y + 3,
      [{new_number, x, y} | acc_numbers],
      acc_symbols
    )
  end

  defp locate_elements([[a, b | more_columns] | more_rows], x, y, acc_numbers, acc_symbols)
       when is_integer(a) and is_integer(b) do
    new_number = String.to_integer("#{a}#{b}")

    locate_elements(
      [more_columns | more_rows],
      x,
      y + 2,
      [{new_number, x, y} | acc_numbers],
      acc_symbols
    )
  end

  defp locate_elements([[a | more_columns] | more_rows], x, y, acc_numbers, acc_symbols)
       when is_integer(a) do
    new_number = String.to_integer("#{a}")

    locate_elements(
      [more_columns | more_rows],
      x,
      y + 1,
      [{new_number, x, y} | acc_numbers],
      acc_symbols
    )
  end

  defp locate_elements([["." | more_columns] | more_rows], x, y, acc_numbers, acc_symbols) do
    locate_elements([more_columns | more_rows], x, y + 1, acc_numbers, acc_symbols)
  end

  defp locate_elements([[symbol | more_columns] | more_rows], x, y, acc_numbers, acc_symbols) do
    locate_elements([more_columns | more_rows], x, y + 1, acc_numbers, [
      {symbol, x, y} | acc_symbols
    ])
  end

  defp filter_adjacent(_numbers, [], acc), do: acc

  defp filter_adjacent(numbers, [{_symbol, sx, sy} | more_symbols], acc) do
    more_acc = Enum.filter(numbers, &is_adjacent?(&1, {sx, sy}))
    filter_adjacent(numbers, more_symbols, more_acc ++ acc)
  end

  # credo:disable-for-lines:5 Credo.Check.Refactor.CyclomaticComplexity
  defp is_adjacent?({number, x, y}, {sx, sy}) do
    (number < 10 and sx in (x - 1)..(x + 1) and sy in (y - 1)..(y + 1)) or
      (number >= 10 and number < 100 and sx in (x - 1)..(x + 1) and sy in (y - 1)..(y + 2)) or
      (number >= 100 and number < 1000 and sx in (x - 1)..(x + 1) and sy in (y - 1)..(y + 3))
  end

  @doc """
  Finds gears, given a schematic.

  ## Examples

      iex> Advent03.gears([[ 4 , 6 , 7 ,".",".", 1 , 1 , 4 ,".","."],
      ...>                 [".",".",".","*",".",".",".",".",".","."],
      ...>                 [".",".", 3 , 5 ,".",".", 6 , 3 , 3 ,"."],
      ...>                 [".",".",".",".",".",".","#",".",".","."],
      ...>                 [ 6 , 1 , 7 ,"*",".",".",".",".",".","."],
      ...>                 [".",".",".",".",".","+",".", 5 , 8 ,"."],
      ...>                 [".",".", 5 , 9 , 2 ,".",".",".",".","."],
      ...>                 [".",".",".",".",".",".", 7 , 5 , 5 ,"."],
      ...>                 [".",".",".","$",".","*",".",".",".","."],
      ...>                 [".", 6 , 6 , 4 ,".", 5 , 9 , 8 ,".","."]])
      [[{598, 9, 5}, {755, 7, 6}], [{35, 2, 2}, {467, 0, 0}]]
  """
  @spec gears(list(list(String.t()))) :: list({String.t(), integer(), integer()})
  def gears(schematic) do
    {numbers, symbols} = locate_elements(schematic, 0, 0, [], [])

    symbols
    |> Enum.filter(fn {symbol, _x, _y} -> symbol == "*" end)
    |> Enum.map(&find_adjacent(&1, numbers))
    |> Enum.filter(fn adjacent_numbers -> length(adjacent_numbers) == 2 end)
  end

  defp find_adjacent({_, sx, sy}, numbers) do
    Enum.filter(numbers, &is_adjacent?(&1, {sx, sy}))
  end

  @doc """
  Calculates gears ratio, given a schematic.

  ## Examples

      iex> Advent03.gear_ratio([[ 4 , 6 , 7 ,".",".", 1 , 1 , 4 ,".","."],
      ...>                      [".",".",".","*",".",".",".",".",".","."],
      ...>                      [".",".", 3 , 5 ,".",".", 6 , 3 , 3 ,"."],
      ...>                      [".",".",".",".",".",".","#",".",".","."],
      ...>                      [ 6 , 1 , 7 ,"*",".",".",".",".",".","."],
      ...>                      [".",".",".",".",".","+",".", 5 , 8 ,"."],
      ...>                      [".",".", 5 , 9 , 2 ,".",".",".",".","."],
      ...>                      [".",".",".",".",".",".", 7 , 5 , 5 ,"."],
      ...>                      [".",".",".","$",".","*",".",".",".","."],
      ...>                      [".", 6 , 6 , 4 ,".", 5 , 9 , 8 ,".","."]])
      467835

  """
  @spec gear_ratio(list(list(String.t()))) :: integer()
  def gear_ratio(schematic) do
    gears(schematic)
    |> Enum.reduce(0, fn [{a, _, _}, {b, _, _}], acc -> a * b + acc end)
  end
end
