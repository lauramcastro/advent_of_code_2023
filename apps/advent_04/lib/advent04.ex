defmodule Advent04 do
  @moduledoc """
  Documentation for `Advent04`.
  """

  @doc """
  Finds the winning numbers out of a given scratchcard.

  ## Examples

      iex> Advent04.winning_numbers("Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53")
      [48, 83, 86, 17]

      iex> Advent04.winning_numbers("Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19")
      [32, 61]

      iex> Advent04.winning_numbers("Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1")
      [1, 21]

      iex> Advent04.winning_numbers("Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83")
      [84]

      iex> Advent04.winning_numbers("Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36")
      []

      iex> Advent04.winning_numbers("Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11")
      []

  """
  @spec winning_numbers(String.t()) :: list(integer())
  def winning_numbers(scratchcard) do
    [winners, players] =
      Regex.split(~r/Card(\s)+(\d)+: /, scratchcard, trim: true) |> hd() |> String.split(" | ")

    winning_numbers = String.split(winners) |> Enum.map(&String.to_integer(&1))
    playing_numbers = String.split(players) |> Enum.map(&String.to_integer(&1))
    Enum.filter(winning_numbers, fn n -> Enum.member?(playing_numbers, n) end)
  end

  @doc """
  Calculates the score of a given scratchcard.

  ## Examples

      iex> Advent04.points(["Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53"])
      8

      iex> Advent04.points(["Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19"])
      2

      iex> Advent04.points(["Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1"])
      2

      iex> Advent04.points(["Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83"])
      1

      iex> Advent04.points(["Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36"])
      0

      iex> Advent04.points(["Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"])
      0

      iex> Advent04.points(["Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53",
      ...>                  "Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19",
      ...>                  "Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1",
      ...>                  "Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83",
      ...>                  "Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36",
      ...>                  "Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"])
      13

  """
  @spec points(list(String.t())) :: integer()
  def points(scratchcards) do
    scratchcards
    |> Enum.reduce(
      0,
      fn card, acc ->
        length = winning_numbers(card) |> length()

        cond do
          length == 0 -> 0
          length == 1 -> 1
          length > 1 -> Integer.pow(2, length - 1)
        end + acc
      end
    )
  end

  @doc """
  Calculates the total number of scratchcards obtained from of a given set of scratchcards.

  ## Examples

      iex> Advent04.scratchcards(["Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53",
      ...>                        "Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19",
      ...>                        "Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1",
      ...>                        "Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83",
      ...>                        "Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36",
      ...>                        "Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"])
      30

  """
  @spec scratchcards(list(String.t())) :: integer()
  def scratchcards(original_scratchcards) do
    do_scratchcards(original_scratchcards, original_scratchcards, 1)
  end

  defp do_scratchcards(_deck, [], acc), do: acc - 1

  defp do_scratchcards(deck, [card_number | rest], acc) when is_integer(card_number) do
    scratchcard = Enum.at(deck, card_number - 1)
    length = winning_numbers(scratchcard) |> length()

    if length > 0,
      do:
        do_scratchcards(
          deck,
          rest ++ Enum.to_list((card_number + 1)..(card_number + length)),
          acc + 1
        ),
      else: do_scratchcards(deck, rest, acc + 1)
  end

  defp do_scratchcards(deck, [scratchcard | rest], acc) do
    length = winning_numbers(scratchcard) |> length()

    if length > 0,
      do: do_scratchcards(deck, rest ++ Enum.to_list((acc + 1)..(acc + length)), acc + 1),
      else: do_scratchcards(deck, rest, acc + 1)
  end

  @doc """
  Calculates the total number of scratchcards obtained from of a given set of scratchcards (fast and scalable).

  ## Examples

      iex> Advent04.scratchcards_fast(["Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53",
      ...>                             "Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19",
      ...>                             "Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1",
      ...>                             "Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83",
      ...>                             "Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36",
      ...>                             "Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"])
      30

  """
  @spec scratchcards_fast(list(String.t())) :: integer()
  def scratchcards_fast(original_scratchcards) do
    original_wins =
      original_scratchcards
      |> Enum.map(fn scratchcard -> winning_numbers(scratchcard) |> length() end)

    accumulate_wins(original_wins, 1, Map.from_keys(Enum.to_list(1..length(original_wins)), 1))
  end

  defp accumulate_wins([], _n, mapped_cards), do: mapped_cards |> Map.values() |> Enum.sum()

  defp accumulate_wins([c | rest], n, mapped_cards) do
    new_mapped_cards = accumulate(mapped_cards, n + 1, n + c, mapped_cards[n])
    accumulate_wins(rest, n + 1, new_mapped_cards)
  end

  defp accumulate(map, s, f, _v) when s > f, do: map
  defp accumulate(map, s, f, v) when s <= f, do: accumulate(%{map | s => map[s] + v}, s + 1, f, v)
end
