defmodule Advent04Test do
  use ExUnit.Case
  doctest Advent04

  test "answer to first challenge" do
    answer = Utils.get_input("priv/input") |> Advent04.points()

    assert answer == 25_231
  end

  test "answer to second challenge" do
    answer = Utils.get_input("priv/input") |> Advent04.scratchcards_fast()

    assert answer == 9_721_255
  end
end
