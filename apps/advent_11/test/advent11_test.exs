defmodule Advent11Test do
  use ExUnit.Case
  doctest Advent11

  test "answer to first challenge" do
    map_str = Utils.get_input("priv/input", fn s -> String.codepoints(s) end)
    answer = Advent11.shortest_paths(map_str)

    assert answer == 10_490_062
  end

  test "answer to second challenge" do
    map_str = Utils.get_input("priv/input", fn s -> String.codepoints(s) end)
    answer = Advent11.shortest_paths(map_str, 1_000_000)

    assert answer == 382_979_724_122
  end
end
