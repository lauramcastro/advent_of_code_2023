defmodule Advent11 do
  @moduledoc """
  Documentation for `Advent11`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Advent11.shortest_paths([[".", ".", ".", "#", ".", ".", ".", ".", ".", "."],
      ...>                          [".", ".", ".", ".", ".", ".", ".", "#", ".", "."],
      ...>                          ["#", ".", ".", ".", ".", ".", ".", ".", ".", "."],
      ...>                          [".", ".", ".", ".", ".", ".", ".", ".", ".", "."],
      ...>                          [".", ".", ".", ".", ".", ".", "#", ".", ".", "."],
      ...>                          [".", "#", ".", ".", ".", ".", ".", ".", ".", "."],
      ...>                          [".", ".", ".", ".", ".", ".", ".", ".", ".", "#"],
      ...>                          [".", ".", ".", ".", ".", ".", ".", ".", ".", "."],
      ...>                          [".", ".", ".", ".", ".", ".", ".", "#", ".", "."],
      ...>                          ["#", ".", ".", ".", "#", ".", ".", ".", ".", "."]])
      374

      iex> Advent11.shortest_paths([[".", ".", ".", "#", ".", ".", ".", ".", ".", "."],
      ...>                          [".", ".", ".", ".", ".", ".", ".", "#", ".", "."],
      ...>                          ["#", ".", ".", ".", ".", ".", ".", ".", ".", "."],
      ...>                          [".", ".", ".", ".", ".", ".", ".", ".", ".", "."],
      ...>                          [".", ".", ".", ".", ".", ".", "#", ".", ".", "."],
      ...>                          [".", "#", ".", ".", ".", ".", ".", ".", ".", "."],
      ...>                          [".", ".", ".", ".", ".", ".", ".", ".", ".", "#"],
      ...>                          [".", ".", ".", ".", ".", ".", ".", ".", ".", "."],
      ...>                          [".", ".", ".", ".", ".", ".", ".", "#", ".", "."],
      ...>                          ["#", ".", ".", ".", "#", ".", ".", ".", ".", "."]], 10)
      1030


      iex> Advent11.shortest_paths([[".", ".", ".", "#", ".", ".", ".", ".", ".", "."],
      ...>                          [".", ".", ".", ".", ".", ".", ".", "#", ".", "."],
      ...>                          ["#", ".", ".", ".", ".", ".", ".", ".", ".", "."],
      ...>                          [".", ".", ".", ".", ".", ".", ".", ".", ".", "."],
      ...>                          [".", ".", ".", ".", ".", ".", "#", ".", ".", "."],
      ...>                          [".", "#", ".", ".", ".", ".", ".", ".", ".", "."],
      ...>                          [".", ".", ".", ".", ".", ".", ".", ".", ".", "#"],
      ...>                          [".", ".", ".", ".", ".", ".", ".", ".", ".", "."],
      ...>                          [".", ".", ".", ".", ".", ".", ".", "#", ".", "."],
      ...>                          ["#", ".", ".", ".", "#", ".", ".", ".", ".", "."]], 100)
      8410

  """
  @spec shortest_paths(list(list(String.t())), non_neg_integer()) :: non_neg_integer()
  def shortest_paths(galaxymap, galaxy_size \\ 2)

  def shortest_paths(galaxymap, galaxy_size) do
    galaxies = process_map_spec(galaxymap, 0, 0, [])
    {raw_rows, raw_columns} = Enum.unzip(galaxies)

    {rows, columns} =
      {raw_rows |> Enum.uniq() |> Enum.sort(), raw_columns |> Enum.uniq() |> Enum.sort()}

    empty_rows = Enum.to_list(Enum.min(rows)..Enum.max(rows)) -- rows
    empty_columns = Enum.to_list(Enum.min(columns)..Enum.max(columns)) -- columns

    galaxies = expand_galaxies(galaxies, empty_rows, empty_columns, galaxy_size, [])

    compute_path_pairs(galaxies, 0)
  end

  defp process_map_spec([], _, _, galaxies), do: galaxies

  defp process_map_spec([[] | more_rows], x, _y, galaxies),
    do: process_map_spec(more_rows, x + 1, 0, galaxies)

  defp process_map_spec([["#" | more_c] | more_rows], x, y, galaxies),
    do: process_map_spec([more_c | more_rows], x, y + 1, [{x, y} | galaxies])

  defp process_map_spec([["." | more_c] | more_rows], x, y, galaxies),
    do: process_map_spec([more_c | more_rows], x, y + 1, galaxies)

  defp expand_galaxies([], _, _, _, acc), do: acc

  defp expand_galaxies([{x, y} | more_g], empty_rows, empty_columns, galaxy_size, acc) do
    expanded_x = x + Enum.count(empty_rows, fn r -> r < x end) * (galaxy_size - 1)
    expanded_y = y + Enum.count(empty_columns, fn c -> c < y end) * (galaxy_size - 1)

    expand_galaxies(more_g, empty_rows, empty_columns, galaxy_size, [
      {expanded_x, expanded_y} | acc
    ])
  end

  defp compute_path_pairs([], acc), do: acc

  defp compute_path_pairs([g | more_g], acc) do
    # we do a partial sum here instead of afterwards
    distances = Enum.reduce(more_g, 0, fn mg, d -> distance(g, mg) + d end)
    compute_path_pairs(more_g, acc + distances)
  end

  defp distance({x1, y1}, {x2, y2}) do
    abs(x1 - x2) + abs(y1 - y2)
  end
end
