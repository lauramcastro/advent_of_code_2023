defmodule Advent06Test do
  use ExUnit.Case
  doctest Advent06

  test "answer to first challenge" do
    inputs = [{60, 475}, {94, 2138}, {78, 1015}, {82, 1650}]
    answer = inputs |> Advent06.count_beat_record()

    assert answer == 345_015
  end

  test "answer to second challenge" do
    answer = Advent06.count_beat_record(60_947_882, 475_213_810_151_650)

    assert answer == 42_588_603
  end
end
