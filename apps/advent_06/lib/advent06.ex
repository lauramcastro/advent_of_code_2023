defmodule Advent06 do
  @moduledoc """
  Documentation for `Advent06`.
  """

  @doc """
  Calculates the number of possibilities for beating up a given record.

  ## Examples

      iex> Advent06.count_beat_record(7, 9)
      4

      iex> Advent06.count_beat_record(15, 40)
      8

      iex> Advent06.count_beat_record(30, 200)
      9

      iex> Advent06.count_beat_record(71530, 940200)
      71503

  """
  @spec count_beat_record(integer(), integer()) :: integer()
  def count_beat_record(time, distance) do
    do_count_beat_record({0, time}, time, distance, 0)
  end

  defp do_count_beat_record({time, 0}, time, _distance, acc), do: acc

  defp do_count_beat_record({a, b}, time, distance, acc) when a * b > distance,
    do: do_count_beat_record({a + 1, b - 1}, time, distance, acc + 1)

  defp do_count_beat_record({a, b}, time, distance, acc),
    do: do_count_beat_record({a + 1, b - 1}, time, distance, acc)

  @doc """
  Multiplies the number of possibilities for beating up a given record, given a set of races.

  ## Examples

      iex> Advent06.count_beat_record([{7, 9}, {15, 40}, {30, 200}])
      288

  """
  @spec count_beat_record(list({integer(), integer()})) :: integer()
  def count_beat_record(races),
    do:
      races
      |> Enum.map(fn {time, distance} -> Advent06.count_beat_record(time, distance) end)
      |> Enum.reduce(1, fn x, acc -> x * acc end)
end
