defmodule Advent09Test do
  use ExUnit.Case
  doctest Advent09

  test "answer to first challenge" do
    answer =
      Utils.get_input("priv/input", fn s ->
        String.split(s) |> Enum.map(&String.to_integer(&1))
      end)
      |> Advent09.sum_of_predictions()

    assert answer == 1_993_300_041
  end

  test "answer to second challenge" do
    answer =
      Utils.get_input("priv/input", fn s ->
        String.split(s) |> Enum.map(&String.to_integer(&1))
      end)
      |> Advent09.sum_of_predictions(:previous)

    assert answer == 1_038
  end
end
