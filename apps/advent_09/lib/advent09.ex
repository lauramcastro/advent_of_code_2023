defmodule Advent09 do
  @moduledoc """
  Documentation for `Advent09`.
  """

  @doc """
  Predicts the (default next, possibly previous) value in a given series.

  ## Examples

      iex> Advent09.predict([0, 3, 6, 9, 12, 15])
      18

      iex> Advent09.predict([1, 3, 6, 10, 15, 21])
      28

      iex> Advent09.predict([10, 13, 16, 21, 30, 45])
      68

      iex> Advent09.predict([0, 3, 6, 9, 12, 15], :previous)
      -3

      iex> Advent09.predict([1, 3, 6, 10, 15, 21], :previous)
      0

      iex> Advent09.predict([10, 13, 16, 21, 30, 45], :previous)
      5

  """
  @spec predict(list(integer()), :next | :previous) :: integer()
  def predict(sequence, mode \\ :next)
  def predict(sequence, :next), do: do_predict(sequence, [], [Enum.reverse(sequence)])
  def predict(sequence, :previous), do: Enum.reverse(sequence) |> do_predict([], [sequence])

  defp do_predict([_], differences, sequences),
    do:
      if(Enum.all?(differences, fn n -> n == 0 end),
        do: Enum.reduce(sequences, 0, fn [h | _rest], acc -> h + acc end),
        else: do_predict(Enum.reverse(differences), [], [differences | sequences])
      )

  defp do_predict([a, b | more], differences, sequences),
    do: do_predict([b | more], [b - a | differences], sequences)

  @doc """
  Sums the predicted next values for a given series.

  ## Examples

      iex> Advent09.sum_of_predictions([[0, 3, 6, 9, 12, 15],
      ...>                              [1, 3, 6, 10, 15, 21],
      ...>                              [10, 13, 16, 21, 30, 45]])
      114

      iex> Advent09.sum_of_predictions([[0, 3, 6, 9, 12, 15],
      ...>                              [1, 3, 6, 10, 15, 21],
      ...>                              [10, 13, 16, 21, 30, 45]], :previous)
      2

  """
  @spec sum_of_predictions(list(list(integer())), :next | :previous) :: integer()
  def sum_of_predictions(sequences, mode \\ :next)

  def sum_of_predictions(sequences, mode),
    do: sequences |> Enum.reduce(0, fn s, acc -> predict(s, mode) + acc end)
end
