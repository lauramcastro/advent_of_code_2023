defmodule UtilsTest do
  use ExUnit.Case
  doctest Utils

  test "reads from test file" do
    assert Utils.get_input("priv/input") == ["2023"]
  end
end
