defmodule Advent01Test do
  use ExUnit.Case
  doctest Advent01

  test "answer to first challenge" do
    answer = Utils.get_input("priv/input") |> Advent01.sum_of_calibration_values()
    assert answer == 54_601
  end

  test "answer to second challenge" do
    answer = Utils.get_input("priv/input") |> Advent01.sum_of_calibration_values(true)
    assert answer == 54_078
  end
end
