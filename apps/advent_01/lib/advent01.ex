defmodule Advent01 do
  @moduledoc """
  Documentation for `Advent01`.
  """

  @digits ["1", "2", "3", "4", "5", "6", "7", "8", "9"]

  @doc """
  The calibration value can be found by combining the first digit and the last digit (in that order) to form a single two-digit number.

  ## Examples

      iex> Advent01.calibration_value("1abc2")
      12

      iex> Advent01.calibration_value("pqr3stu8vwx")
      38

      iex> Advent01.calibration_value("a1b2c3d4e5f")
      15

      iex> Advent01.calibration_value("treb7uchet")
      77

      iex> Advent01.calibration_value("two1nine", true)
      29

      iex> Advent01.calibration_value("eightwothree", true)
      83

      iex> Advent01.calibration_value("abcone2threexyz", true)
      13

      iex> Advent01.calibration_value("xtwone3four", true)
      24

      iex> Advent01.calibration_value("4nineeightseven2", true)
      42

      iex> Advent01.calibration_value("zoneight234", true)
      14

      iex> Advent01.calibration_value("7pqrstsixteen", true)
      76

  """
  @spec calibration_value(String.t(), boolean()) :: integer()
  def calibration_value(sequence, transliterate \\ false) do
    bits = sequence |> String.codepoints()
    first = bits |> find_first(transliterate)
    last = Enum.reverse(bits) |> find_last(transliterate)
    String.to_integer(first <> last)
  end

  defp find_first(list, false), do: do_find(list)
  defp find_first(list, true), do: do_find_more(list)

  defp find_last(list, false), do: do_find(list)
  defp find_last(list, true), do: do_find_rev_more(list)

  defp do_find([c | rest]), do: if(c in @digits, do: c, else: do_find(rest))

  defp do_find_more(["o", "n", "e" | _rest]), do: "1"
  defp do_find_more(["t", "w", "o" | _rest]), do: "2"
  defp do_find_more(["t", "h", "r", "e", "e" | _rest]), do: "3"
  defp do_find_more(["f", "o", "u", "r" | _rest]), do: "4"
  defp do_find_more(["f", "i", "v", "e" | _rest]), do: "5"
  defp do_find_more(["s", "i", "x" | _rest]), do: "6"
  defp do_find_more(["s", "e", "v", "e", "n" | _rest]), do: "7"
  defp do_find_more(["e", "i", "g", "h", "t" | _rest]), do: "8"
  defp do_find_more(["n", "i", "n", "e" | _rest]), do: "9"
  defp do_find_more([c | rest]), do: if(c in @digits, do: c, else: do_find_more(rest))

  defp do_find_rev_more(["e", "n", "o" | _rest]), do: "1"
  defp do_find_rev_more(["o", "w", "t" | _rest]), do: "2"
  defp do_find_rev_more(["e", "e", "r", "h", "t" | _rest]), do: "3"
  defp do_find_rev_more(["r", "u", "o", "f" | _rest]), do: "4"
  defp do_find_rev_more(["e", "v", "i", "f" | _rest]), do: "5"
  defp do_find_rev_more(["x", "i", "s" | _rest]), do: "6"
  defp do_find_rev_more(["n", "e", "v", "e", "s" | _rest]), do: "7"
  defp do_find_rev_more(["t", "h", "g", "i", "e" | _rest]), do: "8"
  defp do_find_rev_more(["e", "n", "i", "n" | _rest]), do: "9"
  defp do_find_rev_more([c | rest]), do: if(c in @digits, do: c, else: do_find_rev_more(rest))

  @doc """
  Sum of calibration values.

  ## Examples

      iex> Advent01.sum_of_calibration_values(["1abc2", "pqr3stu8vwx", "a1b2c3d4e5f", "treb7uchet"])
      142

      iex> Advent01.sum_of_calibration_values(["two1nine", "eightwothree", "abcone2threexyz", "xtwone3four", "4nineeightseven2", "zoneight234", "7pqrstsixteen"], true)
      281

  """
  @spec sum_of_calibration_values(list(String.t()), boolean()) :: integer()
  def sum_of_calibration_values(sequences, transliterate \\ false),
    do: Enum.reduce(sequences, 0, fn s, acc -> calibration_value(s, transliterate) + acc end)
end
