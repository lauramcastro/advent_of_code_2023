defmodule Advent12Test do
  use ExUnit.Case
  doctest Advent12

  @tag :skip
  test "answer to first challenge" do
    answer = Utils.get_input("priv/input") |> Enum.map(&Advent12.arrangements/1) |> Enum.sum()

    # too high
    assert answer == 17_186
  end

  @tag :skip
  test "answer to second challenge" do
    answer = Utils.get_input("priv/input")

    assert answer == :unknown
  end
end
