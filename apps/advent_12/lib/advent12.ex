defmodule Advent12 do
  @moduledoc """
  Documentation for `Advent12`.
  """

  @doc """
  Manages patterns of broken springs.

  ## Examples

      iex> Advent12.arrangements("???.### 1,1,3")
      1

      iex> Advent12.arrangements(".??..??...?##. 1,1,3")
      4

      iex> Advent12.arrangements("?#?#?#?#?#?#?#? 1,3,1,6")
      1

      iex> Advent12.arrangements("????.#...#... 4,1,1")
      1

      iex> Advent12.arrangements("????.######..#####. 1,6,5")
      4

      > Advent12.arrangements("?###???????? 3,2,1")
      10

      iex> Advent12.arrangements("??????? 2,1")
      10

  """
  @spec arrangements(String.t()) :: non_neg_integer()
  def arrangements(spring_pattern) do
    [char_pattern, number_pattern] = String.split(spring_pattern)

    incomplete_pattern =
      char_pattern
      |> String.trim_leading(".")
      |> String.trim_trailing(".")
      |> String.codepoints()
      |> clean_pattern([])

    complete_pattern =
      number_pattern
      |> String.codepoints()
      |> fill_pattern([])
      |> Enum.join()
      |> String.trim_leading(".")
      |> String.trim_trailing(".")
      |> String.codepoints()
      |> clean_pattern([])

    matches(incomplete_pattern, complete_pattern)
  end

  defp clean_pattern([], acc), do: Enum.reverse(acc)
  defp clean_pattern([".", "." | rest], acc), do: clean_pattern(["." | rest], acc)
  defp clean_pattern([s | rest], acc), do: clean_pattern(rest, [s | acc])

  defp fill_pattern([], acc), do: Enum.reverse(acc)
  defp fill_pattern(["," | rest], acc), do: fill_pattern(rest, ["." | acc])

  defp fill_pattern([num_str | rest], acc) do
    num = String.to_integer(num_str)
    filling = ("." <> String.duplicate("#", num) <> ".") |> String.codepoints()
    fill_pattern(rest, filling ++ acc)
  end

  defp matches(a, b) when length(a) == length(b), do: if(compatible(a, b), do: 1, else: 0)

  defp matches(["?" | rest_a], b), do: matches(rest_a, b) + matches(["#" | rest_a], b)
  defp matches([c | rest_a], [c | rest_b]), do: matches(rest_a, rest_b)
  defp matches(a, b), do: if(compatible(a, b), do: 1, else: 0)

  defp compatible([], []), do: true
  defp compatible(["#" | rest_a], ["#" | rest_b]), do: compatible(rest_a, rest_b)
  defp compatible(["." | rest_a], ["." | rest_b]), do: compatible(rest_a, rest_b)
  defp compatible(["?" | rest_a], [_ | rest_b]), do: compatible(rest_a, rest_b)
  defp compatible(_, _), do: false
end
